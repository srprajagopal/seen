package main
import (
    "bytes"
    "encoding/json"
    "time"
    "log"
    "net/http"
    "fmt"
    "github.com/manifoldco/promptui"
    "strings"
    "github.com/pkg/browser"
    "os"
    "github.com/zalando/go-keyring"
    "os/user"
)

func prettyPrint(i interface{}) string {
    s, _ := json.MarshalIndent(i, "", "  ")
    return string(s)
}

type GenericJson struct {
    Obj map[string]interface{}
}

type Document struct {
    Title []string `json:"title"`
}

type Links struct {
    Html string `json:"html"`
    Incontext string `json:"incontext"`
    Json string `json:"json"`
}

type Selector struct {
    Type string `json:"type"`
    EndOffset float64 `json:"endOffset"`
    StartOffset float64 `json:"startOffset"`
    EndContainer float64 `json:"endContainer"`
    StartContainer float64 `json:"startContainer"`
    End float64 `json:"end"`
    Start float64 `json:"start"`
    Exact string `json:"exact"`
    Prefix string `json:"prefix"`
    Suffix string `json:"suffix"`
}

type Target struct {
    Source string `json:"source"`
    Selectors []Selector `json:"selector"`
}

type Annotation struct {
    Text string `json:"text"`
    Uri string `json:"uri"`
    Document Document `json:"document"`
    Target []Target `json:"target"`
    Links Links `json:"links"`
    Highlight string 
    ReadableTitle string
}

type Group struct {
    Total float64 `json:"total"`
    Rows []Annotation `json:"rows"`
}

type GroupMetadata struct {
    Id string `json:"id"`
    Name string `json:"name"`
    Type string `json:"type"`
}

type UserGroups struct {
    Gm []GroupMetadata
}

func logError(err error) {
    if err != nil {
        log.Fatalln(err)
    }
}

func (a *Annotation) exactConcat() {
    a.Highlight = ""
    for _, t := range a.Target {
        for _, s := range t.Selectors {
            a.Highlight = fmt.Sprintf("%s\n%s", a.Highlight, s.Exact)
        }
    }
}

func (a *Annotation) makeReadableTitle() {
    a.ReadableTitle = ""
    for _, t := range a.Document.Title {
        a.ReadableTitle = fmt.Sprintf("%s %s", a.ReadableTitle, t)
    }
}

func createClient() (http.Client) {
    timeout := time.Duration(5 * time.Second)
    client := http.Client {
        Timeout : timeout,
    }
    return client
}

func createRequest(url string, token string) (*http.Request) {
    requestBody, err := json.Marshal(map[string]string{})
    logError(err)
    request, err := http.NewRequest("GET", url, bytes.NewBuffer(requestBody))
    logError(err)
    setHeaders(request, token)
    return request
}

func setHeaders(request *http.Request, token string) {
    request.Header.Set("Content-type", "application/json")
    request.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36")
    request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
}

func sendRequest(client *http.Client, request *http.Request)(*http.Response){
    resp, err := client.Do(request)
    logError(err)
    return resp
}

func (g *Group) parseResponse(resp *http.Response){

    dec := json.NewDecoder(resp.Body)
    dec.Decode(&g)
}

func (ug *UserGroups) parseResponse(resp *http.Response)(){
    dec := json.NewDecoder(resp.Body)
    dec.Decode(&ug.Gm)
}

func (g *Group) fetch(client *http.Client, gid string, token string) {
    log.Println(fmt.Sprintf("Fetching group %s", gid))
    url := fmt.Sprintf("https://api.hypothes.is/api/search?group=%s", gid)
    request := createRequest(url, token)
    response := sendRequest(client, request)
    defer response.Body.Close()
    g.parseResponse(response)
}

func (ug *UserGroups) fetch(client *http.Client, token string) {
    url := "https://api.hypothes.is/api/groups"
    request := createRequest(url, token)
    response := sendRequest(client, request)
    defer response.Body.Close()
    ug.parseResponse(response)
}

func displayAnnotations(groups []Group) (string) {
    templates := &promptui.SelectTemplates{
        Label:    "{{ . }}?",
        Active:   "\U000021D2 {{ index .ReadableTitle | green }} ",
        Inactive: "  {{ .ReadableTitle }} ",
        Selected: "\U000021D2 {{ .ReadableTitle }}",
        Details: `
--------- Pepper ----------
{{ "Highlight:" | faint }}   {{ .Highlight }}
{{ "Uri:" | faint }}  {{ .Uri }}
{{ "Title:" | faint }}    {{ .ReadableTitle }}`,
    }

    annotations := []Annotation{}
    for _, group := range groups {
        for _, annotation := range group.Rows {
            annotation.exactConcat()
            annotation.makeReadableTitle()
            annotations = append(annotations, annotation)
        }
    }

    searcher := func(input string, index int) bool {
        annotation := annotations[index]
        text := fmt.Sprintf("%s%s%s", strings.Replace(strings.ToLower(annotation.Highlight), " ", "", -1), strings.Replace(strings.ToLower(annotation.Uri), " ", "", -1), strings.Replace(strings.ToLower(annotation.Text), " ", "", -1))
        input = strings.Replace(strings.ToLower(input), " ", "", -1)
        return strings.Contains(text, input)
    }
        
    prompt := promptui.Select{
        Label : "Annotations",
        Items : annotations,
        Templates : templates,
        Size : len(annotations),
        Searcher: searcher,
    }

    i, _, err := prompt.Run()

    if err != nil {
        fmt.Printf("Prompt failed %v\n", err)
        os.Exit(1)
    }

    return annotations[i].Links.Incontext
}

func main() {

    devurl := "https://hypothes.is/account/developer"

    // get the user
    user, err := user.Current()
    if err != nil {
        log.Println("Cannot find your username for token fetch.")
        os.Exit(1)
    }
    
    // check if keyring available
    token, err := keyring.Get("seen", user.Username)

    if err != nil {
        fmt.Printf("No access token found for user %s\n", user.Username)
        fmt.Printf("User access token can be found in: %s\n", devurl)
        prompt_token := promptui.Prompt{
            Label:    "Hypothesis Token",
        }
    
        token, err = prompt_token.Run()
        
        if err != nil {
            fmt.Printf("Your input failed\n")
            os.Exit(1)
        }
        fmt.Printf("Saving this token to keyring for user %s\n", user.Username)
        keyring.Set("seen", user.Username, token)
    }

    fmt.Printf("Using the saved token for user %s ... \n", user.Username)

    client := createClient()
    ug := UserGroups{}
    ug.fetch(&client, token)
    var groups []Group
    for _, gm := range ug.Gm {
        if gm.Id != "__world__" {
            g := Group{}
            g.fetch(&client, gm.Id, token)
            groups = append(groups, g)
        }
    }

    for {
        link := displayAnnotations(groups)
        log.Println(fmt.Sprintf("Opening link %s ...", link))
        browser.OpenURL(link)
    }
}
