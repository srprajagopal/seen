Seen
=

A command line lookup for [hypothes.is](https://hypothes.is) annotations. Reduces context switch. Search for annotations right from your terminal. When you find the right one, select to open in the browser.

Installation
=

`go get gitlab.com/srprajagopal/seen`

Usage
=

Type `seen` from your terminal. The first time, you need to paste the access token from the [hypothesis developr page](https://hypothes.is/account/developer). The token is saved in the OS keyring using [go-keyring](https://github.com/zalando/go-keyring). 

keybindings
--
Use `/` to search your annotations.
Use arrow keys to navigate through the list
